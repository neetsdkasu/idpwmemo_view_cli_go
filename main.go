package main

import (
	"bitbucket.org/neetsdkasu/idpwmemo_go"
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func main() {
	if err := run(); err != nil {
		log.Panic(err)
	}
}

func run() error {
	switch len(os.Args) {
	case 2:
		file := os.Args[1]
		return listUpServices(file)
	case 3:
		file, serviceName := os.Args[1], os.Args[2]
		return viewService(file, serviceName, 1)
	case 4:
		file, serviceName := os.Args[1], os.Args[2]
		var nth int
		if _, err := fmt.Sscan(os.Args[3], &nth); err != nil {
			println("[ERROR]: <n-th> is not number")
			showUsage()
			return nil
		}
		return viewService(file, serviceName, nth)
	default:
		showUsage()
		return nil
	}
}

func showUsage() {
	exe := filepath.Base(os.Args[0])

	const usage = `%[1]s
  IDPWMemo data file viewer
usage
  %[1]s <memo_file>
    list up service names in the memo
  %[1]s <memo_file> <service_name>
    show the values of first service found to partially match <service_name>
  %[1]s <memo_file> <service_name> <n-th>
    show the values of <n-th> service found to partially match <service_name>
`

	fmt.Printf(usage, exe)
}

func getPassword() (string, error) {
	fmt.Print("password: ")
	s := bufio.NewScanner(os.Stdin)
	if !s.Scan() {
		return "", s.Err()
	}
	fmt.Println()
	return s.Text(), nil
}

func listUpServices(file string) error {
	buf, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}
	password, err := getPassword()
	if err != nil {
		return err
	}
	idpwMemo := idpwmemo_go.NewIDPWMemo()
	if err = idpwMemo.SetPassword([]byte(password)); err != nil {
		return err
	}
	if err = idpwMemo.LoadMemo(buf); err != nil {
		return err
	}
	names, err := idpwMemo.ServiceNames()
	if err != nil {
		return err
	}
	for i, n := range names {
		fmt.Printf("%3d: %s\n", i, n)
	}
	return nil
}

func viewService(file, serviceName string, nth int) error {
	buf, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}
	password, err := getPassword()
	if err != nil {
		return err
	}
	idpwMemo := idpwmemo_go.NewIDPWMemo()
	if err = idpwMemo.SetPassword([]byte(password)); err != nil {
		return err
	}
	if err = idpwMemo.LoadMemo(buf); err != nil {
		return err
	}
	names, err := idpwMemo.ServiceNames()
	if err != nil {
		return err
	}
	index := -1
	for i, n := range names {
		if strings.Contains(n, serviceName) {
			index = i
			nth--
			if nth == 0 {
				break
			}
		}
	}
	if index < 0 {
		return fmt.Errorf("not found service_name (%s)", serviceName)
	}
	if err = idpwMemo.SelectService(index); err != nil {
		return err
	}
	values, err := idpwMemo.Values()
	if err != nil {
		return err
	}
	fmt.Println("[VALUES]")
	for _, v := range values.Slice() {
		fmt.Printf("  %s:\n", v.ValueType)
		fmt.Printf("    %s\n", v.Value)
	}
	secrets, err := idpwMemo.Secrets()
	if err != nil {
		return err
	}
	fmt.Println("[SECRETS]")
	for _, v := range secrets.Slice() {
		fmt.Printf("  %s:\n", v.ValueType)
		fmt.Printf("    %s\n", v.Value)
	}
	service, err := idpwMemo.GetSelectedService()
	if err != nil {
		return err
	}
	unixMilliTime := service.Time
	fmt.Println("[LAST UPDATE]")
	if unixMilliTime == 0 {
		fmt.Println("  ????-??-?? ??:??:??")
	} else {
		t := time.Unix(unixMilliTime/1000, (unixMilliTime%1000)*1000000)
		fmt.Println(t.Format("  2006-01-02 15:04:05"))
	}
	return nil
}
