IDPWMemo-View-CLI-Go
====================


IDPWMemoのデータ閲覧CLI




IDPWMemoのデータのサンプルファイル`sample.memo`のパスワードは`さんぷるdayo`　
※パスワードはUTF-8のエンコードで入力する必要がある  　




実行例1  
```bash
$ idpwmemo_view_cli_go sample.memo
password: さんぷるdayo

  0: SAMPLE Service
  1: サンプル！
  2: simple-sample
```



実行例2  
```bash
$ idpwmemo_view_cli_go sample.memo "SAMPLE"
password: さんぷるdayo

[VALUES]
  service name:
    SAMPLE Service
  id:
    user1
  service url:
    http://example.com
  email:
    user1@example.com
[SECRETS]
  password:
    Password1
```



実行例3   
```bash
$ echo さんぷるdayo > sample_password
$ idpwmemo_view_cli_go sample.memo < sample_password
password: 
  0: SAMPLE Service
  1: サンプル！
  2: simple-sample
```
